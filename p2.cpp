#define OSCPKT_OSTREAM_OUTPUT
#include "oscpkt.hh"
#include "udp.hh"
#include<stdlib.h>
using namespace std;
using namespace oscpkt;
const int PORT_NUM = 8338;
const int PRESS=10;

int main()
{
    int press=-1;
    int pressCount;
    cout << "Server started, will listen to packets on port " << PORT_NUM << std::endl;
    PacketReader pr;
    PacketWriter pw;
    UdpSocket sock; 
    sock.bindTo(PORT_NUM);
    while (sock.isOk()) {      
        if (sock.receiveNextPacket(30 /* timeout, in ms */)) {
            pr.init(sock.packetData(), sock.packetSize());
            oscpkt::Message *msg;
            while (pr.isOk() && (msg = pr.popMessage()) != 0) {
                float iarg;
                if (msg->match("/pose/orientation").isOk()) {
                    //                            cout << "Server: received /ping " << iarg << " from " << sock.packetOrigin() << "\n";
                    cout << *msg << "\n";
                    float x,y;
                    msg->match("/pose/orientation").popFloat(x).popFloat(y);
                    cerr<<"  "<<y<<"  \n";
                    cerr<<press<<endl;
                    if(y>0.1){
                        if(press!=0){
                            system("xdotool keydown Right;xdotool keyup Right;");
                            press=0;
                            pressCount=PRESS;
                        }
                        else if(pressCount==0){
                            system("xdotool keydown Right;xdotool keyup Right;");
                        }
                        else{
                            --pressCount;
                        }
                        cerr<<")";
                    }
                    else if(y<-0.1){
                        if(press!=1){
                            system("xdotool keydown Left;xdotool keyup Left;");
                            press=1;
                            pressCount=PRESS;
                        }
                        else if(pressCount==0){
                            system("xdotool keydown Left;xdotool keyup Left;");
                        }
                        else{
                            --pressCount;
                        }
                        cerr<<"(";
                    }
                    else {
                        if(x<-0.15){
                            if(press!=2){
                                system("xdotool keydown Up;xdotool keyup Up;");
                                press=2;
                                pressCount=PRESS;
                            }
                            else if(pressCount==0){
                                system("xdotool keydown Up;xdotool keyup Up;");
                            cerr<<"?";
                                
                            }
                            else{
                                --pressCount;
                            }
                            cerr<<"?";
                            //    press=true;
                        }
                        else if(x>0.15){
                            if(press!=3){
                                system("xdotool keydown Down;xdotool keyup Down;");
                                press=3;
                                pressCount=PRESS;
                            }
                            else if(pressCount==0){
                                system("xdotool keydown Down;xdotool keyup Down;");
                            }
                            else{
                                --pressCount;
                            }
                            cerr<<"!";
                            //     press=true;
                        }
                        else{
                            press=-1;
                        }
                    }

                    //                            
                } else {
                    //                            cout << "Server: unhandled message: " << *msg << "\n";
                }
            }
        }
    }

    return 0;
}
